import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {
  
  authors:string[] = ['Lewis Carrol','Leo Tolstoy', 'Thomas Mann', 'George R R Martin'];
  test;
  getAuthors(){
    const authorsObservable = new Observable(
      observer => {
        setInterval(
          () => observer.next(this.authors),4000
        )
      }
    )
    return authorsObservable;
    }

    addAuthors(name){
      this.authors.push(name);
       }
  constructor() { }
}