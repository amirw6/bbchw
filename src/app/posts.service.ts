import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Posts } from './interfaces/posts';
import { map } from 'rxjs/operators';
import { Users } from './interfaces/users';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class postsService {

  apiURLposts = "https://jsonplaceholder.typicode.com/posts/";
  apiURLusers = "https://jsonplaceholder.typicode.com/users/";
  
  

  constructor(private http:HttpClient,private db:AngularFirestore) { 
  }
  /*
  getUsers(): Observable<Users>{
    return this.http.get<Users>(this.apiURLusers)
  }

  searchPostData(): Observable<Posts>{
    return this.http.get<Posts>(this.apiURLposts)
    
    }

    */
 /* getPostsDb():Observable<any[]>{
    return this.db.collection('posts').valueChanges();
  }
  addPostsDb(title:string,author:string){
    const post = {title:title, author:author};
    this.db.collection('posts').add(post);
   }
*/
  /*getPosts() {
  return this.http.get<Posts[]>(this.apiURLposts);}
  getUsers() {
  return this.http.get<Users[]>(this.apiURLusers);  
  }*/

  getPosts():Observable<any[]>{
    const ref = this.db.collection('posts');
    return ref.valueChanges({idField: 'id'});
  }
  getPost(id:string):Observable<any>{
    return this.db.doc(`posts/${id}`).get();
  }

  addPosts(title:string, author:string, body:string){
    const post = {body:body, author:author, title:title};
    this.db.collection('posts').add(post);
  }

  addPost(title:string, author:string, body:string){
    const post = {title:title, author:author, body:body}
    this.db.collection('posts').add(post);
  }

  deletePost(id:string){
    this.db.doc(`posts/${id}`).delete();
  }

  updatePost(id:string,title:string, author:string, body:string){
    this.db.doc(`posts/${id}`).update({
      title:title,
      author:author, 
      body:body
    })
  }

}
